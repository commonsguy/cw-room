/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.fts

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.InputStream

private const val BOOK_DIR = "TheTimeMachine"

class BookRepository(
  private val db: BookDatabase,
  private val context: Context
) {
  suspend fun all(): List<String> {
    val result = db.bookStore().all()

    return if (result.isEmpty()) {
      import()
      db.bookStore().all()
    } else {
      result
    }
  }

  suspend fun filtered(search: String): List<String> =
    db.bookStore().filtered(search)

  private suspend fun import() = withContext(Dispatchers.IO) {
    val assets = context.assets

    val entities = assets.list(BOOK_DIR).orEmpty()
      .sorted()
      .map { paragraphs(assets.open("$BOOK_DIR/$it")) }
      .flatten()
      .mapIndexed { index, prose -> ParagraphEntity(0, index, prose) }

    db.bookStore().insert(entities)
  }
}

// inspired by https://stackoverflow.com/a/10065920/115145

private fun paragraphs(stream: InputStream) =
  paragraphs(stream.reader().readLines())

private fun paragraphs(lines: List<String>) =
  lines.fold(listOf<String>()) { roster, line ->
    when {
      line.isEmpty() -> roster + ""
      roster.isEmpty() -> listOf(line)
      else -> roster.take(roster.size - 1) + (roster.last() + " ${line.trim()}")
    }
  }
    .map { it.trim() }
    .filter { it.isNotEmpty() }
