## Elements of Android Room

[*Elements of Android Room*](https://commonsware.com/Room/)
is a book focusing the Room component of the Android Jetpack, offering an object-based
API for working with SQLite for local relational data storage.

This repository contains the source code for the sample apps profiled in the book. These 
samples are updated as the book is, with `git` tags applied to tie sample code versions to book
versions.

The book, and the samples, were written by Mark Murphy. You may also have run into him through
Stack Overflow:

<a href="https://stackoverflow.com/users/115145/commonsware">
<img src="https://stackoverflow.com/users/flair/115145.png" width="208" height="58" alt="profile for CommonsWare at Stack Overflow, Q&amp;A for professional and enthusiast programmers" title="profile for CommonsWare at Stack Overflow, Q&amp;A for professional and enthusiast programmers">
</a>

All of the source code in this archive is licensed under the
Apache 2.0 license except as noted.
