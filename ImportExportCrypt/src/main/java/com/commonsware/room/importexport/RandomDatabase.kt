/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.importexport

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import net.sqlcipher.database.SupportFactory
import java.io.File
import java.io.InputStream
import java.io.OutputStream

private const val DB_NAME = "random.db"
private const val PASSPHRASE = "sekr1t"

@Database(entities = [RandomEntity::class], version = 1)
@TypeConverters(TypeTransmogrifier::class)
abstract class RandomDatabase : RoomDatabase() {
  abstract fun randomStore(): RandomStore

  companion object {
    fun newInstance(context: Context) =
      Room.databaseBuilder(context, RandomDatabase::class.java, DB_NAME)
        .openHelperFactory(SupportFactory(PASSPHRASE.toByteArray()))
        .build()

    fun exists(context: Context) = context.getDatabasePath(DB_NAME).exists()

    fun copyTo(context: Context, stream: OutputStream) {
      context.getDatabasePath(DB_NAME).inputStream().copyTo(stream)
    }

    fun copyFrom(context: Context, stream: InputStream) {
      val dbFile = context.getDatabasePath(DB_NAME)

      dbFile.delete()

      stream.copyTo(dbFile.outputStream())
    }

    fun decryptTo(context: Context, stream: OutputStream) {
      val temp = File(context.cacheDir, "export.db")

      temp.delete()
      
      SQLCipherUtils.decryptTo(
        context,
        context.getDatabasePath(DB_NAME),
        temp,
        PASSPHRASE.toByteArray()
      )

      temp.inputStream().copyTo(stream)
      temp.delete()
    }

    fun encryptFrom(context: Context, stream: InputStream) {
      val temp = File(context.cacheDir, "import.db")

      temp.delete()

      stream.copyTo(temp.outputStream())

      try {
        when (SQLCipherUtils.getDatabaseState(context, temp)) {
          SQLCipherUtils.State.UNENCRYPTED -> SQLCipherUtils.encryptTo(
            context,
            temp,
            context.getDatabasePath(DB_NAME),
            PASSPHRASE.toByteArray()
          )
          SQLCipherUtils.State.DOES_NOT_EXIST ->
            throw IllegalStateException("Could not find $temp???")
          SQLCipherUtils.State.ENCRYPTED ->
            throw IllegalStateException("Original database appears encrypted!")
        }
      } finally {
        temp.delete()
      }
    }
  }
}