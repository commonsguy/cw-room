/*
  Copyright (c) 2020-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.importexport

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.commonsware.room.importexport.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

private val DATE_TIME_FORMATTER =
  DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
    .withZone(ZoneId.systemDefault())
private const val DEFAULT_EXPORT_TITLE = "ImportExport.db"

class MainActivity : AppCompatActivity() {
  private val openDoc =
    registerForActivityResult(ActivityResultContracts.OpenDocument()) { uri ->
      vm.import(uri)
    }

  private val createDoc =
    registerForActivityResult(ActivityResultContracts.CreateDocument()) { uri ->
      vm.export(uri)
    }

  private val vm: MainViewModel by viewModel()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    vm.viewStates.observe(this) { state ->
      when (state) {
        MainViewState.Loading -> {
          binding.progress.isVisible = true
          binding.content.isVisible = false
          binding.state.isVisible = false
        }
        is MainViewState.Content -> {
          binding.progress.isVisible = false
          binding.content.isVisible = true
          binding.state.isVisible = true
          updateContent(state.summary, binding)
        }
        MainViewState.Error -> {
          binding.progress.isVisible = false
          binding.content.isVisible = false
          binding.state.isVisible = true
          binding.state.setText(R.string.error)
        }
      }
    }

    binding.populate.setOnClickListener { vm.populate() }

    binding.exportDatabase.setOnClickListener {
      createDoc.launch(DEFAULT_EXPORT_TITLE)
    }

    binding.importDatabase.setOnClickListener {
      openDoc.launch(arrayOf("application/octet-stream"))
    }
  }

  private fun updateContent(summary: Summary, binding: ActivityMainBinding) {
    if (summary.count > 0) {
      val oldest = DATE_TIME_FORMATTER.format(summary.oldestTimestamp)

      binding.state.text =
        getString(R.string.summaryTemplate, summary.count, oldest)
      binding.exportDatabase.isEnabled = true
    } else {
      binding.state.setText(R.string.empty)
      binding.exportDatabase.isEnabled = false
    }
  }
}
