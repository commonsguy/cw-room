/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.natpryce.hamkrest.hasSize
import com.natpryce.hamkrest.isEmpty

import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppEntityTest {
  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.apps()

  @Test
  fun queryDisplayModels() {
    assertThat(underTest.loadAll(), isEmpty)
    assertThat(underTest.loadListModels(), isEmpty)

    val fdroid = AppEntity(
      applicationId = "org.fdroid.fdroid",
      displayName = "F-Droid",
      shortDescription = "An independent app store featuring open source Android apps",
      fullDescription = "F-Droid is an installable catalogue of FOSS (Free and Open Source Software) applications for the Android platform. The client makes it easy to browse, install, and keep track of updates on your device. Visit https://f-droid.org to learn more!",
      lastUpdated = 1566652015000,
      latestVersionName = "1.7.1",
      donationUrl = "https://flattr.com/thing/343053/F-Droid-Repository",
      packageUrl = "https://f-droid.org/FDroid.apk",
      iconUrl = "https://gitlab.com/fdroid/fdroidclient/raw/master/app/src/main/res/drawable-hdpi/ic_launcher.png?inline=true"
    )

    underTest.insert(fdroid)

    underTest.loadAll().let {
      assertThat(it, hasSize(equalTo(1)))
      assert(it[0] == fdroid)
    }

    underTest.loadListModels().let {
      assertThat(it, hasSize(equalTo(1)))

      val model = it[0]

      assertThat(model.applicationId, equalTo(fdroid.applicationId))
      assertThat(model.displayName, equalTo(fdroid.displayName))
      assertThat(model.shortDescription, equalTo(fdroid.shortDescription))
      assertThat(model.iconUrl, equalTo(fdroid.iconUrl))
    }
  }
}
