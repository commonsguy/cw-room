/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Room_

  https://commonsware.com/Room
*/

package com.commonsware.room.misc.manytomany

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.commonsware.room.misc.MiscDatabase
import com.natpryce.hamkrest.*
import com.natpryce.hamkrest.assertion.assertThat
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ManyToManyTest {
  @get:Rule
  val instantExecutorRule = InstantTaskExecutorRule()

  private val db = Room.inMemoryDatabaseBuilder(
    InstrumentationRegistry.getInstrumentation().targetContext,
    MiscDatabase::class.java
  )
    .build()
  private val underTest = db.bookstoreManyToMany()

  @Test
  fun relations() = runBlockingTest {
    val category =
      Category(shortCode = "stuff", displayName = "Books About Stuff")
    val bookOne = Book(
      isbn = "035650056X",
      title = "Feed"
    )
    val bookTwo = Book(
      isbn = "0451459792",
      title = "Dies the Fire"
    )

    val testJob = launch {
      assertThat(underTest.loadAll(), isEmpty)

      underTest.insertAll(category, bookOne, bookTwo)

      val all = underTest.loadAll()

      assertThat(all, hasSize(equalTo(1)))
      assertThat(all[0].category, equalTo(category))
      assertThat(
        all[0].books,
        allOf(hasSize(equalTo(2)), hasElement(bookOne), hasElement(bookTwo))
      )

      val loaded = underTest.loadByShortCode(category.shortCode)

      assertThat(loaded.category, equalTo(category))
      assertThat(
        loaded.books,
        allOf(hasSize(equalTo(2)), hasElement(bookOne), hasElement(bookTwo))
      )

      underTest.delete(bookOne)

      val postDeleteAll = underTest.loadAll()

      assertThat(postDeleteAll, hasSize(equalTo(1)))
      assertThat(postDeleteAll[0].category, equalTo(category))
      assertThat(
        postDeleteAll[0].books,
        allOf(hasSize(equalTo(1)), hasElement(bookTwo))
      )
    }

    testJob.join()
  }
}
